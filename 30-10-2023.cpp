#include <iostream>

void isAllergicTo(int test);
void allergensList(bool eggs, bool peanuts, bool shellfish, bool strawberries, bool tomatoes, bool chocolate, bool pollen, bool cats);

int main()
{
	unsigned short int test;

	std::cin >> test;

	isAllergicTo(test);
}
unsigned short int degreeOfTwo(int degree)
{
	unsigned short int number{ 1 };

	for (int i = 0; i < degree; i++)
	{
		number *= 2;
	}

	return number;
}

void isAllergicTo(int test)
{
	bool eggs{ 0 };
	bool peanuts{ 0 };
	bool shellfish{ 0 };
	bool strawberries{ 0 };
	bool tomatoes{ 0 };
	bool chocolate{ 0 };
	bool pollen{ 0 };
	bool cats{ 0 };

	if ( (test & degreeOfTwo(0)) == degreeOfTwo(0))
	{
		eggs = 1;
	}
	if ( (test & degreeOfTwo(1)) == degreeOfTwo(1))
	{
		peanuts = 1;
	}
	if ( (test & degreeOfTwo(2)) == degreeOfTwo(2) )
	{
		shellfish = 1;
	}
	if ( (test & degreeOfTwo(3)) == degreeOfTwo(3) )
	{
		strawberries = 1;
	}
	if ( (test & degreeOfTwo(4)) == degreeOfTwo(4) )
	{
		tomatoes = 1;
	}
	if ( (test & degreeOfTwo(5)) == degreeOfTwo(5))
	{
		chocolate = 1;
	}
	if ( (test & degreeOfTwo(6)) == degreeOfTwo(6) )
	{
		pollen = 1;
	}
	if ( (test & degreeOfTwo(7)) == degreeOfTwo(7) )
	{
		cats = 1;
	}

	allergensList(eggs, peanuts, shellfish, strawberries, tomatoes, chocolate, pollen, cats);
}

void allergensList(bool eggs, bool peanuts, bool shellfish, bool strawberries, bool tomatoes, bool chocolate, bool pollen, bool cats)
{
	std::cout                   << "Our test give us next result: ";
	if (eggs) std::cout         << " eggs ";
	if (peanuts) std::cout      << " peanuts ";
	if (shellfish) std::cout    << " shellfish ";
	if (strawberries) std::cout << " strawberries ";
	if (tomatoes) std::cout     << " tomatoes ";
	if (chocolate) std::cout    << " chocolate ";
	if (pollen) std::cout       << " pollen ";
	if (cats) std::cout         << " cats ";
}
